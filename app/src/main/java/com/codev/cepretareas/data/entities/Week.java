package com.codev.cepretareas.data.entities;

import java.io.Serializable;

public class Week implements Serializable {
    private int id;
    private String nameWeek;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameWeek() {
        return nameWeek;
    }

    public void setNameWeek(String nameWeek) {
        this.nameWeek = nameWeek;
    }
}
