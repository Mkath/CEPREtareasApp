package com.codev.cepretareas.data.entities;

import java.io.Serializable;

/**
 * Created by manu on 04/08/16.
 */
public class AccessTokenEntity implements Serializable {
    private String token;
    private UserEntity user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getAccessToken() {
        return token;
    }
}
