package com.codev.cepretareas.data.entities;

import java.io.Serializable;

public class NotificationEntity implements Serializable {
    private String titulo;
    private String descripcion;
    private String img_anuncio;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImg_anuncio() {
        return img_anuncio;
    }

    public void setImg_anuncio(String img_anuncio) {
        this.img_anuncio = img_anuncio;
    }
}
