package com.codev.cepretareas.data.remote.request;


import com.codev.cepretareas.data.entities.AccessTokenEntity;
import com.codev.cepretareas.data.entities.UserEntity;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by katherine on 10/05/17.
 */

public interface LoginRequest {
    @FormUrlEncoded
    @POST("listado/usuarios/0")
    Call<AccessTokenEntity> login(@Field("userEmail") String email,
                                  @Field("userPassword") String password);

    @GET("listado/usuarios/0")
    Call<ArrayList<UserEntity>> loginUser();


}
