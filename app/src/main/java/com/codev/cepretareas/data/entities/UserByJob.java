package com.codev.cepretareas.data.entities;

import java.io.Serializable;

/**
 * Created by junior on 30/09/16.
 */
public class UserByJob implements Serializable {

   private String UserName;

   private String UserLastName1;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserLastName1() {
        return UserLastName1;
    }

    public void setUserLastName1(String userLastName1) {
        UserLastName1 = userLastName1;
    }
}
