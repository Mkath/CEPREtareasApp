package com.codev.cepretareas.data.entities.response;

import java.io.Serializable;

public class ResponseRegister implements Serializable {
    private int resultado;
    private String mensaje;

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
