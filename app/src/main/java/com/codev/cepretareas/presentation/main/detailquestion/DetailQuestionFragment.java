package com.codev.cepretareas.presentation.main.detailquestion;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codev.cepretareas.R;
import com.codev.cepretareas.core.BaseActivity;
import com.codev.cepretareas.core.BaseFragment;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.presentation.profile.ProfileActivity;
import com.codev.cepretareas.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class DetailQuestionFragment extends BaseFragment implements DetailQuestionContract.View {

    @BindView(R.id.im_question)
    ImageView imQuestion;
    @BindView(R.id.im_solution)
    ImageView imSolution;
    @BindView(R.id.rating)
    RatingBar rating;
    Unbinder unbinder;
    @BindView(R.id.tv_points)
    TextView tvPoints;
    private DetailQuestionContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private SessionManager mSessionManager;
    private AlertDialog dialogSend;

    private ArrayList<Week> listWeek;
    private ArrayList<QuestionEntity> listQuestions, listQuestionByWeek,
            listQuestionByType;

    private int idWeek;
    private String title;

    private QuestionEntity questionEntity;

    public DetailQuestionFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static DetailQuestionFragment newInstance(Bundle bundle) {
        DetailQuestionFragment fragment = new DetailQuestionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new DetailQuestionPresenter(this, getContext());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_detail_homework, container, false);
        unbinder = ButterKnife.bind(this, root);
        questionEntity = (QuestionEntity) getArguments().get("questionEntity");
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");
        Glide.with(getContext()).load(questionEntity.getImgProblema()).into(imQuestion);

    }

    private void updateRating(){
        if (questionEntity.getImgSolucion() == null) {
            rating.setEnabled(false);
            Toast.makeText(getContext(), "Solución aún no habilitada", Toast.LENGTH_SHORT).show();
            tvPoints.setTextColor(getResources().getColor(R.color.gray));
        }else{
            rating.setEnabled(true);
            if(questionEntity.getCalificacion() == 0.0){
                tvPoints.setText("¡Califica la solución!");
            }
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void getCalificationResponse() {
        rating.setEnabled(false);

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(DetailQuestionContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }


}
