package com.codev.cepretareas.presentation.main.notificationlist;

import android.content.Context;

import com.codev.cepretareas.data.entities.NotificationEntity;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.data.remote.ServiceFactory;
import com.codev.cepretareas.data.remote.request.ListRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class NotificationPresenter implements NotificationContract.Presenter {

    private final NotificationContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;

    public NotificationPresenter(NotificationContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
    }



    @Override
    public void getListNotification(int idCiclo) {
            mView.setLoadingIndicator(true);
            final ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
            final Call<ArrayList<NotificationEntity>> reservation = listRequest.getNotifications(idCiclo);
            reservation.enqueue(new Callback<ArrayList<NotificationEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<NotificationEntity>> call, Response<ArrayList<NotificationEntity>> response) {
                    mView.setLoadingIndicator(false);
                    if (!mView.isActive()) {
                        return;
                    }
                    if (response.isSuccessful()) {
                       mView.getListNotification(response.body());
                        //mView.getListHomework(response.body());

                    } else {
                        mView.showErrorMessage("Error al obtener la lista");
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<NotificationEntity>> call, Throwable t) {
                    if (!mView.isActive()) {
                        return;
                    }
                    mView.setLoadingIndicator(false);
                    mView.showErrorMessage("Error al conectar con el servidor");
                }
            });
        }
}
