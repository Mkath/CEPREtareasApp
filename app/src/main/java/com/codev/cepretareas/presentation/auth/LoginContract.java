package com.codev.cepretareas.presentation.auth;

import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;
import com.codev.cepretareas.data.entities.AccessTokenEntity;
import com.codev.cepretareas.data.entities.UserEntity;

/**
 * Created by katherine on 12/05/17.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        void loginSuccessful(UserEntity userEntity);
        void errorLogin(String msg);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void loginUser(String username, String password);
        void openSession(UserEntity userEntity);
    }
}
