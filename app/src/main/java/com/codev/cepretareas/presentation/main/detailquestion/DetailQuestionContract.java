package com.codev.cepretareas.presentation.main.detailquestion;


import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;

/**
 * Created by katherine on 31/05/17.
 */

public interface DetailQuestionContract {
    interface View extends BaseView<Presenter> {

     //   void getListHomework(ArrayList<QuestionEntity> list);

        void getCalificationResponse();

        boolean isActive();


    }

    interface Presenter extends BasePresenter {


        void sendCalification(int calification);


    }
}
