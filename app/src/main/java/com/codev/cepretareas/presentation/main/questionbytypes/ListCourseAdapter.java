package com.codev.cepretareas.presentation.main.questionbytypes;

import android.app.Activity;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codev.cepretareas.R;
import com.codev.cepretareas.core.LoaderAdapter;
import com.codev.cepretareas.data.entities.CourseEntity;
import com.codev.cepretareas.data.entities.QuestionEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListCourseAdapter extends LoaderAdapter<CourseEntity> {

    private Context context;
    private String fecha;
    private int accionEstado;
    private ArrayList<QuestionEntity> listQuestion, listQuestionByCourse;
    private QuestionbyTypesAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private Activity activity;



    public ListCourseAdapter(ArrayList<CourseEntity> noticiaEntities, Context context, ArrayList<QuestionEntity> listQuestion, Activity activity) {
        super(context);
        setItems(noticiaEntities);
        this.context = context;
        this.listQuestion = listQuestion;
        this.activity = activity;
        listQuestionByCourse = new ArrayList<>();
    }

    public ArrayList<CourseEntity> getItems() {
        return (ArrayList<CourseEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_course, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final CourseEntity servicioEntity = getItems().get(position);
        ((ViewHolder) holder).tvTitle.setText(servicioEntity.getNombreCurso());
        ((ViewHolder) holder).container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ViewHolder)holder).rvCourse.setVisibility(View.VISIBLE);

                mLayoutManager = new LinearLayoutManager(context);
                ((ViewHolder)holder).rvCourse.setLayoutManager(mLayoutManager);
                mAdapter = new QuestionbyTypesAdapter(new ArrayList<QuestionEntity>(), context, activity);
                ((ViewHolder)holder).rvCourse.setAdapter(mAdapter);

                getListByCourse(listQuestion, servicioEntity.getIdCurso());

            }
        });

        imageByCourse(((ViewHolder) holder), servicioEntity.getIdCurso());

    }

    private void imageByCourse(ViewHolder holder, int idCourse){
        switch (idCourse){
            case 1:
                Glide.with(context).load(R.drawable.ic_practiquemos).into(holder.imLogo);
                break;
            case 2:
                break;
        }
    }
    private void getListByCourse(ArrayList<QuestionEntity> list, int idCourse) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getIdCurso() == idCourse) {
                listQuestionByCourse.add(list.get(i));
            }
        }

        if (listQuestionByCourse.size() == 0) {
            Toast.makeText(context, "Sin contenido", Toast.LENGTH_SHORT).show();
        } else {
            mAdapter.setItems(listQuestionByCourse);
        }

    }

    static class ViewHolder extends RecyclerView.ViewHolder {



        @BindView(R.id.imLogo)
        ImageView imLogo;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.imItem)
        ImageView imItem;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.rvCourse)
        RecyclerView rvCourse;
        @BindView(R.id.container)
        ConstraintLayout container;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }


}
