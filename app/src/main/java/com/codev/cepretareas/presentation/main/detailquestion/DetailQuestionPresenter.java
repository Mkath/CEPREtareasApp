package com.codev.cepretareas.presentation.main.detailquestion;

import android.content.Context;

import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;
import com.codev.cepretareas.data.local.SessionManager;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public class DetailQuestionPresenter implements DetailQuestionContract.Presenter {

    private final DetailQuestionContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    private ArrayList<Week> listWeek = new ArrayList<>();
    private ArrayList<QuestionEntity> listQuestions = new ArrayList<>();

    public DetailQuestionPresenter(DetailQuestionContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
    }
/*

    @Override
    public void loadListHomework(int idUser, int idCiclo) {
        mView.setLoadingIndicator(true);
        final ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        final Call<ArrayList<QuestionEntity>> reservation = listRequest.getListQuestions(idUser, idCiclo);
        reservation.enqueue(new Callback<ArrayList<QuestionEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<QuestionEntity>> call, Response<ArrayList<QuestionEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    listQuestions = response.body();
                        if(listWeek.size() == 0){
                            loadListWeek(listQuestions);

                        }else   {
                            mView.getListWeek(listWeek);
                        }
                    //mView.getListHomework(response.body());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<QuestionEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
*/



    @Override
    public void sendCalification(int calification) {

    }
}
