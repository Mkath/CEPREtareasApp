package com.codev.cepretareas.presentation.main.questionbytypes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.LoaderAdapter;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.presentation.main.detailquestion.DetailQuestionActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionbyTypesAdapter extends LoaderAdapter<QuestionEntity>  {


    private Context context;
    private String fecha;
    private int accionEstado;
    private Activity activity;

    public QuestionbyTypesAdapter(ArrayList<QuestionEntity> noticiaEntities, Context context, Activity activity) {
        super(context);
        setItems(noticiaEntities);
        this.context = context;
        this.activity = activity;
    }

    public ArrayList<QuestionEntity> getItems() {
        return (ArrayList<QuestionEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        final QuestionEntity servicioEntity = getItems().get(position);
        //AGREGE ID PARA VERIFICAR SI SE MOSTRABA LA CANTIDAD ADECUADA
        ((ViewHolder) holder).tvUniversityName.setText(servicioEntity.getId()+servicioEntity.getNombreCurso() + servicioEntity.getNombreSemana());
        ((ViewHolder) holder).container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(activity, DetailQuestionActivity.class);
                intent.putExtra("questionEntity", servicioEntity);
                activity.startActivity(intent);

            }
        });

    }



    static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvUniversityName)
        TextView tvUniversityName;
        @BindView(R.id.container)
        ConstraintLayout container;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }


}
