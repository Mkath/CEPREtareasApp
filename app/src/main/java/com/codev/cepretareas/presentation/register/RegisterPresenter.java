package com.codev.cepretareas.presentation.register;

import android.content.Context;
import android.support.annotation.NonNull;

import com.codev.cepretareas.data.entities.AccessTokenEntity;
import com.codev.cepretareas.data.entities.UserEntity;
import com.codev.cepretareas.data.entities.body.BodyRegister;
import com.codev.cepretareas.data.entities.response.ResponseRegister;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.data.remote.ServiceFactory;
import com.codev.cepretareas.data.remote.request.RegisterRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 3/05/17.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

    private RegisterContract.View mView;
    private Context context;
    private SessionManager mSessionManager;


    public RegisterPresenter(RegisterContract.View mView, Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "view cannot be null!");
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(this.context);
    }


    @Override
    public void start() {

    }

    @Override
    public void registerUser(@NonNull final BodyRegister bodyRegister) {

        final RegisterRequest registerRequest =
                ServiceFactory.createService(RegisterRequest.class);

        Call<ResponseRegister> call = registerRequest.registerUser(bodyRegister);
        mView.setLoadingIndicator(true);
        call.enqueue(new Callback<ResponseRegister>() {
            @Override
            public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {
                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    /*if(response.body().getResultado()==1){
                        mView.registerSuccessful(response.body());
                    }else {
                        mView.errorRegister("Registro fallido");
                    }*/
                    mView.registerSuccessful(response.body());

                } else {
                    mView.setLoadingIndicator(false);
                    mView.errorRegister("Falló el registro, por favor inténtelo nuevamente");
                }
            }
            @Override
            public void onFailure(Call<ResponseRegister> call, Throwable t) {
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Ocurrió un error al tratar de ingresar, por favor intente nuevamente");
            }
        });
    }

}
