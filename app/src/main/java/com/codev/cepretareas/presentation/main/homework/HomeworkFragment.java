package com.codev.cepretareas.presentation.main.homework;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.BaseActivity;
import com.codev.cepretareas.core.BaseFragment;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.presentation.main.questionbytypes.QuestionByTypesActivity;
import com.codev.cepretareas.presentation.profile.ProfileActivity;
import com.codev.cepretareas.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class HomeworkFragment extends BaseFragment implements HomeworkContract.View {


    @BindView(R.id.spinnerWeekend)
    Spinner spinnerWeekend;
    @BindView(R.id.container_practiquemos)
    ConstraintLayout containerPractiquemos;
    @BindView(R.id.container_calculo)
    ConstraintLayout containerCalculo;
    @BindView(R.id.container_pacticas_calificadas)
    ConstraintLayout containerPacticasCalificadas;
    @BindView(R.id.container_simulacros)
    ConstraintLayout containerSimulacros;
    @BindView(R.id.container_seminarios)
    ConstraintLayout containerSeminarios;
    @BindView(R.id.refresh_layout)
    ConstraintLayout refreshLayout;
    Unbinder unbinder;
    private HomeworkContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private SessionManager mSessionManager;
    private AlertDialog dialogSend;

    private ArrayList<Week> listWeek;
    private ArrayList<QuestionEntity> listQuestions, listQuestionByWeek,
            listQuestionByType;

    private int idWeek;
    private String title;

    public HomeworkFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadListHomework(mSessionManager.getUserEntity().getId(), mSessionManager.getUserEntity().getIdCiclo());
    }

    public static HomeworkFragment newInstance() {
        return new HomeworkFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new HomeworkPresenter(this, getContext());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_homework, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");
        listQuestions = new ArrayList<>();
        listQuestionByWeek = new ArrayList<>();
        listQuestionByType = new ArrayList<>();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void getListWeek(final ArrayList<Week> list) {

        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i <list.size() ; i++) {
            names.add(list.get(i).getNameWeek());
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, names);

        spinnerWeekend.setAdapter(adapter);

        spinnerWeekend.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idWeek = list.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void getListHomeworkByWeekAndType(ArrayList<QuestionEntity> list) {
        if(list.size()!=0){
            nextActivity(list, title);

        }else   {
            Toast.makeText(getContext(), "Contenido no disponible, por favor selecciona otra opción.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(HomeworkContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick({R.id.container_practiquemos, R.id.container_calculo, R.id.container_pacticas_calificadas, R.id.container_simulacros, R.id.container_seminarios, R.id.refresh_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.container_practiquemos:
                mPresenter.loadListHomeworkbyWeekAndType(1, idWeek);
                title = "Practiquemos";

                break;
            case R.id.container_calculo:

                mPresenter.loadListHomeworkbyWeekAndType(2, idWeek);
                title = "Cálculo y Resolución de problemas";

                break;
            case R.id.container_pacticas_calificadas:
                mPresenter.loadListHomeworkbyWeekAndType(3, idWeek);
                title = "Prácticas Calificadas";

                break;
            case R.id.container_simulacros:
                mPresenter.loadListHomeworkbyWeekAndType(4, idWeek);
                title = "Simulacros";

                break;
            case R.id.container_seminarios:
                mPresenter.loadListHomeworkbyWeekAndType(5, idWeek);
                title ="Seminarios";
                break;

            case R.id.refresh_layout:
                break;
        }
    }

    private void nextActivity(ArrayList<QuestionEntity> list, String title){
        Intent intent    = new Intent(getActivity(), QuestionByTypesActivity.class);
        intent.putExtra("list", list);
        intent.putExtra("title", title);
        startActivity(intent);
    }
}
