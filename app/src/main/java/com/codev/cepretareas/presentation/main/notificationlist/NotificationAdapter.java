package com.codev.cepretareas.presentation.main.notificationlist;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.LoaderAdapter;
import com.codev.cepretareas.data.entities.NotificationEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends LoaderAdapter<NotificationEntity> {

    private Context context;
    private String fecha;
    private int accionEstado;

    public NotificationAdapter(ArrayList<NotificationEntity> noticiaEntities, Context context) {
        super(context);
        setItems(noticiaEntities);
        this.context = context;
    }

    public ArrayList<NotificationEntity> getItems() {
        return (ArrayList<NotificationEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        final NotificationEntity notificationEntity = getItems().get(position);
        //AGREGE ID PARA VERIFICAR SI SE MOSTRABA LA CANTIDAD ADECUADA
   /*     ((ViewHolder) holder).tvUniversityName.setText(servicioEntity.getId()+servicioEntity.getNombreCurso() + servicioEntity.getNombreSemana());
        ((ViewHolder) holder).container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(activity, DetailQuestionActivity.class);
                intent.putExtra("questionEntity", servicioEntity);
                activity.startActivity(intent);

            }
        });
*/
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDescript)
        TextView tvDescript;
        @BindView(R.id.imNotif)
        ImageView imNotif;
        @BindView(R.id.container)
        ConstraintLayout container;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }


}
