package com.codev.cepretareas.presentation.main.questionbytypes;

import com.codev.cepretareas.data.entities.QuestionEntity;

/**
 * Created by kath on 2/06/18.
 */

public interface QuestionItem {

    void clickItem(QuestionEntity questionEntity);

    void deleteItem(QuestionEntity questionEntity, int position);
}
