package com.codev.cepretareas.presentation.load;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.BaseActivity;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.presentation.auth.LoginActivity;
import com.codev.cepretareas.presentation.main.homework.HomeworkActivity;
import com.codev.cepretareas.utils.GifImageView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by katherine on 12/05/17.
 */

public class LoadActivity extends BaseActivity {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.btnFacebook)
    TextView btnFacebook;
    @BindView(R.id.btnRegister)
    TextView btnRegister;
    @BindView(R.id.btnLogin)
    TextView btnLogin;
    private SessionManager mSessionManager;


    // Set the duration of the splash screen

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_first);
        ButterKnife.bind(this);

        mSessionManager = new SessionManager(this);


        if (mSessionManager.isLogin()) {
            newActivityClearPreview(this, null, HomeworkActivity.class);
        }
    }

    @OnClick({R.id.btnFacebook, R.id.btnRegister, R.id.btnLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnFacebook:
                Toast.makeText(this, "Próximamente", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnRegister:

                break;
            case R.id.btnLogin:
                newActivityClearPreview(this, null, LoginActivity.class);
                break;
        }
    }
}
